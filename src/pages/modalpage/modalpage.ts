import { Component } from '@angular/core';
import { NavController, ToastController, ActionSheetController, NavParams } from 'ionic-angular';
import { formArrayNameProvider } from '@angular/forms/src/directives/reactive_directives/form_group_name';
import { CompileTemplateMetadata } from '@angular/compiler';

/**
 * Generated class for the ModalpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */


@Component({
  selector: 'page-modalpage',
  templateUrl: 'modalpage.html',
})
export class ModalpagePage {

  constructor(public navCtrl: NavController, public toastController : ToastController, public navParams: NavParams, public actionsheetChrl: ActionSheetController) {
  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ModalpagePage');
  }
  closeModal(){
    this.navCtrl.pop();
  }
  submitForm(){

      const actionSheet = this.actionsheetChrl.create({
        title: 'Click add to Submit',
        buttons: [
          {
            text: 'Add',
            role: 'destructive',
            handler: () => {
              let toaster=this.toastController.create({
                message : 'Form Added',
                duration : 2000
                
              });
              toaster.present();
              console.log('Destructive clicked');
            }
          
          },{
            text: 'Cancel',
            role: 'cancel',
            handler: () => {
              let toaster=this.toastController.create({
                message : 'Form canceled',
                duration : 2000
                
              });
              toaster.present();
              console.log('Cancel clicked');
            }
          }
        ]
      });
      actionSheet.present();
      
    }

    
    doRefresh(refresher){
      console.log('begin async operation', refresher);

      setTimeout(()=> {
        console.log('Async operation has ended');
        refresher.complete();
      },2000);

      }

  
    
  }

