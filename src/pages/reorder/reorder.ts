import { Component } from '@angular/core';
import { IonicPage, NavController, AlertController, reorderArray, NavParams } from 'ionic-angular';

/**
 * Generated class for the ReorderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({ 
  selector: 'page-reorder',
  templateUrl: 'reorder.html',
})

export class ReorderPage {
  public addarray= [];
  public reordEnabled = false;
  addText: any;
  event: any;

  constructor(public navCtrl: NavController,private alertController: AlertController, public navParams: NavParams) {
  }

  toggleReorder(){
    this.reordEnabled= !this.reordEnabled;
  }

  itemReordered($event){
   reorderArray(this.addarray, $event);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ReorderPage');
  }

  addalert(){
      let adal = this.alertController.create({
              title: "Env based topics",
              message: "Add your interest",
              inputs:[
                {
                type: "text",
                name: "addlist"
              }],
              buttons:[
                {
                  text:"Cancel"
                },

                {
                  text:"Add",
                  handler:(inputData) =>{
                    let addText;
                    addText = inputData.addlist;
                    this.addarray.push(addText);
                }

            }]
              
      });
       adal.present()
  }

}
