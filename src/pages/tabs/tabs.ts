import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FirstpagePage } from '../firstpage/firstpage';
import { ModalpagePage } from '../modalpage/modalpage';
import { HomePage } from '../home/home';
import { ReorderPage } from '../reorder/reorder';
import { SlidePage } from '../slide/slide';
import { TablePage } from '../table/table';



@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  homePage = HomePage;
  firstPage = FirstpagePage;
  modalPage = ModalpagePage;
  reorderPage = ReorderPage;
  slidePage = SlidePage;
  tablePage = TablePage; 
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
  }

}
