import { Component } from '@angular/core';
import { IonicPage, LoadingController, ActionSheetController, NavController, ModalController,NavParams,AlertController } from 'ionic-angular';
import { ModalpagePage } from '../modalpage/modalpage';
import { SlidePage } from '../slide/slide';
import { ReorderPage } from '../reorder/reorder';
import { TablePage } from '../table/table';



/**
 * Generated class for the FirstpagePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-firstpage',
  templateUrl: 'firstpage.html',
})
export class FirstpagePage {
  connect: string
  public tap: number = 0;
  constructor(public loadController: LoadingController, private modalController: ModalController, public actionsheetCtrl: ActionSheetController, public navCtrl: NavController, public navParams: NavParams, private alertController: AlertController) {
  
    this.connect="Activities";
  }
  modal(){
   let openmodal=this.modalController.create(ModalpagePage);
   openmodal.present();
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad FirstpagePage');
  }
action(){
  const actionSheet = this.actionsheetCtrl.create({
    title: 'Modify your album',
    buttons: [
      {
        text: 'Add',
        role: 'destructive',
        handler: () => {
          console.log('Destructive clicked');
        }
      },{
        text: 'Delete',
        handler: () => {
          console.log('Archive clicked');
        }
      },{
        text: 'Cancel',
        role: 'cancel',
        handler: () => {
          console.log('Cancel clicked');
        }
      }
    ]
  });
  actionSheet.present();
}

slide(){

  this.navCtrl.push(SlidePage);
}

reorder(){
  const loader = this.loadController.create({
    content: "Please wait...",
    duration: 2000
  });
  loader.present();
  this.navCtrl.push(ReorderPage);
}
table(){
  const loader = this.loadController.create({
    content: "Please wait...",
    duration: 2000
  });
  loader.present();
  this.navCtrl.push(TablePage);
}
tapEvent(e) {
  this.tap++
}
}