import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { FirstpagePage } from '../firstpage/firstpage';
import { PopoverController } from 'ionic-angular';
import { PopoverPage } from '../popover/popover';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  public event = {
    month: '1990-02-19',
    timeStarts: '07:43',
    timeEnds: '1990-02-20'
  }
   
  constructor(public popoverCtrl: PopoverController, public navCtrl: NavController) {

  }

  nextpage1(){
            this.navCtrl.push(FirstpagePage);
  }
  
  presentPopover(){
    
    const popover= this.popoverCtrl.create(PopoverPage);
    
    popover.present();
  }

}
