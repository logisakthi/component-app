import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, NavController, IonicErrorHandler, IonicModule,  } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { FirstpagePage } from '../pages/firstpage/firstpage';
import { ModalpagePage } from '../pages/modalpage/modalpage';
import { SlidePage } from '../pages/slide/slide';
import { ReorderPage } from '../pages/reorder/reorder';
import { TablePage } from '../pages/table/table';
import { PopoverPage } from '../pages/popover/popover';
import { TabsPage } from '../pages/tabs/tabs';





@NgModule({
  declarations: [
    MyApp,
    HomePage,
    FirstpagePage,
    ModalpagePage,
    SlidePage,
    ReorderPage,
    TablePage,
    PopoverPage,
    TabsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    FirstpagePage,
    ModalpagePage,
    SlidePage,
    ReorderPage,
    TablePage,
    PopoverPage,
    TabsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {
  
}
